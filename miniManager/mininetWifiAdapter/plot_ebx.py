import sys
sys.path.insert(0, '../mininet-wifi/mn_wifi')

from plot import PlotGraph

import matplotlib.pyplot as plt

class PlotGraphEbx(PlotGraph):
    @classmethod
    def pause(cls, time_step):
        plt.pause(time_step)