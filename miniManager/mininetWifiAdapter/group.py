class Group:
    def __init__(self, groupId, mobModel, mobModelTime, mobParams, **params):
        self.groupId = groupId
        self.mobModel = mobModel
        self.mobModelTime = mobModelTime
        self.mobParams = mobParams
        self.nodes = []
        self.params = params
    
    def addStep(self, mobModel, mobModelTime, mobParams):
        self.mobModel.append(mobModel)
        self.mobModelTime.append(mobModelTime)
        self.mobParams.append(mobParams)