import sys


from mn_wifi.net import Mininet_wifi
from group import Group
from mobility_ebx import model_ebx as MobModel

from mn_wifi.node import Station, Car, OVSKernelAP
from mn_wifi.sixLoWPAN.node import OVSSensor, LowPANNode
from mn_wifi.wwan.node import WWANNode
from mn_wifi.link import WirelessLink
from mn_wifi.wmediumdConnector import snr

class Mininet_wifi_ebx(Mininet_wifi):
    def __init__(self, accessPoint=OVSKernelAP, station=Station, car=Car,
                 sensor=LowPANNode, apsensor=OVSSensor, modem=WWANNode, link=WirelessLink,
                 ssid="new-ssid", mode="g", encrypt="", passwd=None, ieee80211w=None,
                 channel=1, freq=2.4, band=20, wmediumd_mode=snr, roads=0, fading_cof=0,
                 autoAssociation=True, allAutoAssociation=True, autoSetPositions=False,
                 configWiFiDirect=False, config4addr=False, noise_th=-91, cca_th=-90,
                 disable_tcp_checksum=False, ifb=False, client_isolation=False,
                 plot=False, plot3d=False, docker=False, container='mininet-wifi',
                 ssh_user='alpha', rec_rssi=False, iot_module='mac802154_hwsim',
                 wwan_module='wwan_hwsim', json_file=None, ac_method=None, time_step=0.1, outputToFile=False, printCurrentTime=False, **kwargs):
        super().__init__(accessPoint, station, car, sensor, apsensor, modem, link, ssid, mode, encrypt, passwd, ieee80211w,
                 channel, freq, band, wmediumd_mode, roads, fading_cof, autoAssociation, allAutoAssociation, autoSetPositions,
                 configWiFiDirect, config4addr, noise_th, cca_th, disable_tcp_checksum, ifb, client_isolation, plot, plot3d, docker, container,
                 ssh_user, rec_rssi, iot_module, wwan_module, json_file, ac_method, **kwargs)
        self.groups = []
        self.time_step = time_step
        self.outputToFile = outputToFile
        self.printCurrentTime = printCurrentTime

    def addGroup(self, groupId, mobModel, mobModelTime, mobParams=None, **params):
        newGroup = Group(groupId, mobModel, mobModelTime, mobParams, **params)
        self.groups.append(newGroup)
        return newGroup

    def addStation(self, name, cls=None, **params):
        # Call default implementation
        sta = super().addStation(name, cls, **params)

        # Set initial position
        if 'position' in params:
            sta.params['initPos'] = params['position']

        # Add nodes to groups
        if 'groupId' in params:
            for group in self.groups:
                if group.groupId == params['groupId']:
                    group.nodes.append(sta)

        return sta

    def start_mobility(self, **kwargs):
        if self.groups:
            for group in self.groups:
                for node in group.nodes:
                    if not hasattr(node, 'position'):
                        node.position = (0, 0, 0)
                    if not hasattr(node, 'pos'):
                        node.pos = (0, 0, 0)
            MobModel(**kwargs)
        else:
            super().start_mobility(**kwargs)

    def get_mobility_params(self):
        mob_params = super().get_mobility_params()
        mob_params['groups'] = self.groups
        mob_params['time_step'] = self.time_step
        mob_params['outputToFile'] = self.outputToFile
        mob_params['printCurrentTime'] = self.printCurrentTime
        return mob_params
    
    def check_if_mob(self):
        if len(self.groups) != 0:
            mob_params = self.get_mobility_params()
            stat_nodes, mob_nodes = self.get_mob_stat_nodes()
            self.start_mobility(stat_nodes=stat_nodes, mob_nodes=mob_nodes, **mob_params)
        else:
            super().check_if_mob()