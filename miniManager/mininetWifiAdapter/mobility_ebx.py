import sys
sys.path.append('mininet-wifi/mn_wifi')
sys.path.insert(0, '../mininet-wifi/mn_wifi')

from mininet.log import debug

from mobility import Mobility, model, random_direction, random_walk, random_waypoint, truncated_levy_walk, gauss_markov, reference_point_group
from mobility import tvc, coherence_ref_point

from plot_ebx import PlotGraphEbx

from time import sleep, time
import numpy as np
import math
import random

import warnings
warnings.filterwarnings("ignore", category=UserWarning, module="plot*")

class model_ebx(model):

    def models(self, stations=None, aps=None, stat_nodes=None, 
                groups=None, draw=True, seed=1,
               min_wt=1, max_wt=5, max_x=100, max_y=100, **kwargs):
        "Used when a mobility model is set"
        start_time = time()
        np.random.seed(seed)
        self.ac = kwargs.get('ac_method', None)
        n_groups = kwargs.get('n_groups', 1)
        self.stations, self.mobileNodes, self.aps = stations, stations, aps
        self.time_step = kwargs.get('time_step', 0.1)
        
        mob_nodes = []
        for group in groups:
            for node in group.nodes:
                mob_nodes.append(node)

        for node in mob_nodes:
            args = {'min_x': 0, 'min_y': 0,
                    'max_x': max_x, 'max_y': max_y,
                    'min_v': 10, 'max_v': 10}
            for key in args.keys():
                setattr(node, key, node.params.get(key, args[key]))

        if draw:
            nodes = mob_nodes + stat_nodes
            PlotGraphEbx(nodes=nodes, max_x=max_x, max_y=max_y, **kwargs)


        if not groups:
            if draw:
                PlotGraphEbx.pause()
            return

        mob = [-1 for i in range(len(groups))]
        mobility = ["" for _ in range(len(groups))]
        current_time = -1

        while(True):
            if(kwargs.get("printCurrentTime", False)):
                print(current_time)
            
            for i in range(len(groups)):
                mob_currency = mob[i]
                mob[i] = -1
                for j in range(len(groups[i].mobModelTime)):
                    if current_time > groups[i].mobModelTime[j][0] and current_time <= groups[i].mobModelTime[j][1]:
                        mob[i] = j
                        if j == mob_currency:
                            break
                        mobility[i] = self.set_mob_model(groups[i].mobModel[j], groups[i].nodes, n_groups, **groups[i].mobParams[j])
                        break

                if mob[i] == -1:
                    continue
                
                xy =  next(mobility[i])

                if(kwargs.get("outputToFile", False)):
                    f = open("output.txt", "a")
                    f.write(f"{xy[0]}\n")
                    f.close()

                for idx, node in enumerate(groups[i].nodes):
                    pos = round(xy[idx][0], 2), round(xy[idx][1], 2), 0.0
                    self.set_pos(node, pos)
                    if draw:
                        node.update_2d()
            if draw:
                PlotGraphEbx.pause(self.time_step)
            else:
                sleep(0.5)
            while self.pause_simulation:
                pass
            current_time += 1

    
    def set_mob_model(self, mob_model, nodes, n_groups, **kwargs):
        if mob_model == 'RandomWalk':  # Random Walk model
                for node in nodes:
                    array_ = ['constantVelocity', 'constantDistance']
                    for param in array_:
                        if not hasattr(node, param):
                            setattr(node, param, 1)
                return random_walk(nodes)
        elif mob_model == 'TruncatedLevyWalk':  # Truncated Levy Walk model
            return truncated_levy_walk(nodes)
        elif mob_model == 'RandomDirection' and 'max_x' in kwargs and 'max_y' in kwargs:  # Random Direction model
            return random_direction(nodes, dimensions=(kwargs['max_x'], kwargs['max_y']))
        elif mob_model == 'RandomWayPoint' and 'min_wt' in kwargs and 'max_wt' in kwargs:  # Random Waypoint model
            for node in nodes:
                array_ = ['constantVelocity', 'constantDistance', 'min_v', 'max_v']
                for param in array_:
                    if not hasattr(node, param):
                        setattr(node, param, '1')
            return random_waypoint(nodes, wt_min=kwargs['min_wt'], wt_max=kwargs['max_wt'])
        elif mob_model == 'GaussMarkov':  # Gauss-Markov model
            return gauss_markov(nodes, alpha=0.99)
        elif mob_model == 'ReferencePoint':  # Reference Point Group model
            return reference_point_group(nodes, n_groups, dimensions=(kwargs['max_x'], kwargs['max_y']), aggregation=0.5)
        elif mob_model == 'TimeVariantCommunity' and 'max_x' in kwargs and 'max_y' in kwargs:
            return tvc(nodes, n_groups, dimensions=(kwargs['max_x'], kwargs['max_y']), aggregation=[0.5, 0.], epoch=[100, 100])
        elif mob_model == 'CRP' and 'max_x' in kwargs and 'max_y' in kwargs and 'pointlist' in kwargs:
            return coherence_ref_point(nodes, n_groups, (kwargs['max_x'], kwargs['max_y']), kwargs['pointlist'])
        elif mob_model == 'line_formation':
            return line_formation(nodes, kwargs['angle'], kwargs['velocity'], kwargs.get('min_distance', None))
        elif mob_model == 'rotation':
            return rotation(nodes, kwargs['center'], kwargs['velocity'], kwargs.get('min_distance', None))
        elif mob_model == 'wedge_formation':
            return wedge_formation(nodes, kwargs['angle'], kwargs['velocity'], kwargs.get('min_distance', None))
        else:
            raise Exception("Mobility Model not defined or doesn't exist!")

def line_formation(nodes, angle, velocity, minDistance):
    x = np.empty(len(nodes))
    y = np.empty(len(nodes))
    xn = np.empty(len(nodes))
    yn = np.empty(len(nodes))

    # Define minimum distance between nodes
    minDistance = minDistance if minDistance else velocity

    # Defining leader position
    leader = 0
    for i in range(len(nodes)):
        x[i] = nodes[i].position[0]
        y[i] = nodes[i].position[1]
        if nodes[i].params.get("leader", False):
            leader = i
            

    # Calculating movement direction
    direction = (math.cos(angle), math.sin(angle))

    # Sequence of positions
    while True:
        for count in range(len(nodes)):
            i = (leader + count)%len(nodes)
            
            if i == leader:
                xn[i] = x[i] + direction[0]*velocity
                yn[i] = y[i] + direction[1]*velocity
            else:
                prev = (i-1+len(nodes))%len(nodes)

                xd = nodes[prev].position[0] - nodes[i].position[0]
                yd = nodes[prev].position[1] - nodes[i].position[1]
                dist = np.round(np.hypot(abs(xd), abs(yd)), 10)
                if(dist < minDistance):
                    continue

                f_angle = np.arctan2(y[prev] - y[i], x[prev] - x[i])

                x_base = (x[prev] - direction[0]*minDistance)
                y_base = (y[prev] - direction[1]*minDistance)

                x_follow = (x[prev] - np.cos(f_angle)*minDistance)
                y_follow = (y[prev] - np.sin(f_angle)*minDistance)

                base_position = project_position(nodes[i].position[0], nodes[i].position[1], x_base, y_base, velocity, nodes[i].max_v)
                follow_position = project_position(nodes[i].position[0], nodes[i].position[1], x_follow, y_follow, velocity, nodes[i].max_v)

                (xn[i], yn[i]) = weighted_position(base_position, follow_position)

            x[i] = xn[i]
            y[i] = yn[i]

        yield np.dstack((x, y))[0]

def project_position(xc: float, yc: float, xf: float, yf: float, v: float, mv: float):
    """
    project_position calculates the node position based on its velocity and desired destination.
    @xc: current position on x axis.
    @yc: current position on y axis.
    @xf: desired position on x axis.
    @yf: desired position on y axis.
    @v: node velocity.
    @mv: node maximum velocity.
    """
    if(v > mv):
        v = mv

    xd = xf - xc
    yd = yf - yc
    dist = np.round(np.hypot(abs(xd), abs(yd)), 10)

    ang = np.arctan2(yd, xd)

    xp = xc + v*np.cos(ang)
    yp = yc + v*np.sin(ang)
    pred_dist = np.round(np.hypot(abs(xp - xc), abs(yp - yc)), 10)

    if(dist <= pred_dist):
        return (xf, yf)
    
    if(v < mv):
        v = increased_speed(pred_dist, dist, v, mv)

        xp = xc + v*np.cos(ang)
        yp = yc + v*np.sin(ang)
        pred_dist = np.round(np.hypot(abs(xp - xc), abs(yp - yc)), 10)

        if(dist <= pred_dist):
            return (xf, yf)
    
    return (xp, yp)

def increased_speed(pred_dist, dist, v, mv):
    if(dist <= pred_dist):
        return v
    elif(dist > 2*pred_dist):
        return mv
    else:
        return round(dist*(mv-v)/pred_dist + 2*v - mv, 10)

def weighted_position(base_pos, follow_pos, weight=0.5):
    return tuple(np.array(base_pos) * weight + np.array(follow_pos) * (1-weight))

def rotation(nodes, center, velocity, minDistance):
    x = np.empty(len(nodes))
    y = np.empty(len(nodes))
    xn = np.empty(len(nodes))
    yn = np.empty(len(nodes))
    angle = 0
    raio = 0

    leader = 0
    for i in range(len(nodes)):
        x[i] = nodes[i].position[0]
        y[i] = nodes[i].position[1]
        if nodes[i].params.get("leader", False):
            leader = i
            raio = np.hypot(abs(nodes[i].position[0]-center[0]), abs(nodes[i].position[1]-center[1]))
            angle = np.arctan2((nodes[i].position[1]-center[1]), nodes[i].position[0]-center[0])

    while True:
        for count in range(len(nodes)):
            i = (leader + count)%len(nodes)

            if i == leader:
                angle = angle + velocity/raio
                xn[i] = center[0] + raio*math.cos(angle)
                yn[i] = center[1] + raio*math.sin(angle)
            else:
                prev = (i-1+len(nodes))%len(nodes)

                xd = nodes[prev].position[0] - nodes[i].position[0]
                yd = nodes[prev].position[1] - nodes[i].position[1]
                dist = np.round(np.hypot(abs(xd), abs(yd)), 10)
                if(dist < minDistance):
                    continue

                f_angle = np.arctan2(y[prev] - y[i], x[prev] - x[i])

                x_follow = (x[prev] - np.cos(f_angle)*minDistance)
                y_follow = (y[prev] - np.sin(f_angle)*minDistance)

                follow_position = project_position(nodes[i].position[0], nodes[i].position[1], x_follow, y_follow, velocity, nodes[i].max_v)

                (xn[i], yn[i]) = follow_position

            x[i] = xn[i]
            y[i] = yn[i]

        yield np.dstack((x, y))[0]

def wedge_formation(nodes, angle, velocity, minDistance):
    x = np.empty(len(nodes))
    y = np.empty(len(nodes))
    xn = np.empty(len(nodes))
    yn = np.empty(len(nodes))
    positionX = np.empty(len(nodes))
    positionY = np.empty(len(nodes))

    # Defining leader position
    leader = 0
    for i in range(len(nodes)):
        x[i] = nodes[i].position[0]
        y[i] = nodes[i].position[1]
        if nodes[i].params.get("leader", False):
            leader = i

    # Calculating movement direction
    direction = (math.cos(angle), math.sin(angle))
    ortDirection = (math.sin(angle), -math.cos(angle))

    for num in range(len(nodes)):
        i = (leader + num)%len(nodes)
        positionX[i] = x[leader] + ortDirection[0]*minDistance*num
        positionY[i] = y[leader] + ortDirection[1]*minDistance*num

    while True:
        for count in range(len(nodes)):
            i = (leader + count)%len(nodes)

            if i == leader:
                xn[i] = x[i] + direction[0]*velocity
                yn[i] = y[i] + direction[1]*velocity

                for num in range(len(nodes)):
                    j = (leader + num)%len(nodes)
                    positionX[j] = xn[leader] + ortDirection[0]*minDistance*num
                    positionY[j] = yn[leader] + ortDirection[1]*minDistance*num
            else:
                prev = (i-1+len(nodes))%len(nodes)

                follow_position = project_position(nodes[i].position[0], nodes[i].position[1], positionX[i], positionY[i], velocity, nodes[i].max_v)

                (xn[i], yn[i]) = follow_position

            x[i] = xn[i]
            y[i] = yn[i]

        yield np.dstack((x, y))[0] 
